#include <iostream>
#include "id3v2lib.h"
#include "bass.h"
using namespace std;

namespace whtntr {
    class tag_model {
    public:
        float bitrate;
        float freq;
        string title;
        string artist;
        string album;
    };

    class bass_manager {
    public:
        bass_manager();
        int init_bass();
        void set_volume(float volume);
        HSTREAM load_file(string filepath);
        void play(string filename);
        void stop();

        float get_length();
        float get_curr_position();
        void set_position(int position);
        void parse_tags(string filename);

    private:
        HSTREAM stream;
    };
}

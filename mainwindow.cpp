#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->add_filesA, &QAction::triggered, this, &MainWindow::open_files);
    connect(ui->playB, &QPushButton::clicked, this, &MainWindow::play);
    connect(ui->backB, &QPushButton::clicked, this, &MainWindow::prev_track);
    connect(ui->nextB, &QPushButton::clicked, this, &MainWindow::next_track);
    connect(ui->volumeS, &QSlider::valueChanged, this, &MainWindow::volume_changed);
    connect(ui->trackbarS, &QSlider::valueChanged, this, &MainWindow::trackbar_value_changed);
    model = new QStringListModel();
    ui->plLV->setModel(model);

    open_files();

    timer.setInterval(1000);
    connect(&timer, &QTimer::timeout, this, &MainWindow::trackbar_worker);
    timer.start();

}

void MainWindow::open_files()
{
    QStringList files = QFileDialog::getOpenFileNames(this, "open files", ".", "MP3, FLAC (*.mp3)");
    /* QStringList files; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/01. Пыль (ft. Treya).mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/02. Порталы (ft. Tony Raut).mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/03. - Небо солнце.mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/04. Сопротивление (ft. Кэш).mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/05. Танцпол.mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/06. Всего лишь сон.mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/07. Кома (ft. Ai Mori).mp3"; */
    /* files << "/home/sonya/d/mus/mp3/metal/Ravanna - AMOK/08. Амок.mp3"; */
    add_to_playlist(files);
}

void MainWindow::add_to_playlist(QStringList songs_path)
{
    model->setStringList(songs_path);
}

void MainWindow::play() {
    string filename = ui->plLV->currentIndex().data().toString().toStdString();
    bassm.play(filename);
    bassm.parse_tags(filename);
}

void MainWindow::next_track() {

}

void MainWindow::prev_track() {

}

void MainWindow::volume_changed(int volume) {
    bassm.set_volume((float)volume);
}

string MainWindow::format_seconds(int seconds) {
    int minutes = seconds / 60;

    int hours = (minutes >= 60) ? minutes / 60 : 0;
    seconds = seconds % 60;
    string result;
    
    if(hours) {
        if(to_string(hours).length() == 1) 
            result = "0" + to_string(hours) + ":"; 
        else result = to_string(hours) + ":";
    }

    if(to_string(minutes).length() == 1) 
        result += "0" + to_string(minutes) + ":";
    else result += to_string(minutes) + ":";

    if(to_string(seconds).length() == 1) 
        result += "0" + to_string(seconds);
    else result += to_string(seconds);



    return result; 
}

void MainWindow::trackbar_worker() {
    int length = bassm.get_length();
    int curr_pos = bassm.get_curr_position();
    ui->trackbarS->blockSignals(true);
    ui->trackbarS->setMaximum(length);
    ui->trackbarS->setValue(curr_pos);
    ui->trackbarS->blockSignals(false);

    if(length != -1 && curr_pos != -1) {
        ui->timeL->setText(tr(format_seconds(curr_pos).c_str()));
        ui->endL->setText(tr(format_seconds(length).c_str()));
    }
    timer.start(1000);
}

void MainWindow::trackbar_value_changed(int seconds) {
    bassm.set_position(seconds);
}

MainWindow::~MainWindow()
{
    delete ui;
}


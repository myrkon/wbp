#include "bassm.hpp"
using namespace whtntr;

bass_manager::bass_manager() {
    init_bass();
}
int bass_manager::init_bass() {
    int init = BASS_Init(-1, 44100, 0, 0, 0);
    /* BASS_SetVolume(0.5); */
    return init;
}

void bass_manager::set_volume(float volume) {
    BASS_ChannelSetAttribute(stream, BASS_ATTRIB_VOL, volume / 100);
}

HSTREAM bass_manager::load_file(string filepath) {
    stream = BASS_StreamCreateFile(false, filepath.c_str(), 0, 0, 0);
    return stream;
}

void bass_manager::stop() {
    BASS_ChannelStop(stream);
    BASS_StreamFree(stream);
}

void bass_manager::play(string filename) {
    int state = BASS_ChannelIsActive(stream);
    if(state == BASS_ACTIVE_STOPPED) {
        stop();
        init_bass();
        load_file(filename.c_str());
        if(stream != 0) {
            BASS_ChannelPlay(stream, false);
        }
    } else if(state == BASS_ACTIVE_PLAYING) {
        BASS_ChannelPause(stream);
    } else if(state == BASS_ACTIVE_PAUSED) {
        BASS_ChannelPlay(stream, false);
    }
}

float bass_manager::get_length() {
    return BASS_ChannelBytes2Seconds(stream, BASS_ChannelGetLength(stream, 0));
}

float bass_manager::get_curr_position() {
    return BASS_ChannelBytes2Seconds(stream, BASS_ChannelGetPosition(stream, 0));
}

void bass_manager::set_position(int position) {
    BASS_ChannelSetPosition(stream, BASS_ChannelSeconds2Bytes(stream, position), BASS_POS_BYTE);
}

void bass_manager::parse_tags(string filename) {
    ID3v2_tag* tags = load_tag(filename.c_str()); 
    string title, artist, album;

    ID3v2_frame* title_frame = tag_get_artist(tags);
    ID3v2_frame_text_content* title_content = parse_text_frame_content(title_frame);
    cout << title_content->data << "\n";
    /* title  = parse_text_frame_content(tag_get_title(tags ))->data; */
    /* artist = parse_text_frame_content(tag_get_artist(tags))->data; */
    /* album  = parse_text_frame_content(tag_get_album(tags ))->data; */
    
    /* auto album_cover = tag_get_album_cover(tags)->data; */

    /* cout << "title: " << title << "\n"; */
    /* cout << "artist: " << artist << "\n"; */
    /* cout << "album: " << album << "\n"; */
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <thread>
#include <QMainWindow>
#include <QFileDialog>
#include <QDebug>
#include <QListView>
#include <QStringListModel>
#include <QTimer>
#include <vector>

#include "bassm.hpp"

using namespace std;
using namespace whtntr;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class tab
{
public:
    QListView * view;
    QStringListModel * model;
};

/* class trackbar_worker { */
/*     public: */ 
/*         trackbar_worker(bass_manager* bm); */
/*         void do_work(); */
/*     private: */
/*         bass_manager* bassm; */
/* }; */
/* class trackbar_worker : public QObject{ */
/*     Q_OBJECT */
/* public: */
/*     trackbar_worker(bass_manager* bm); */
/* public slots: */
/*     void doWork(); */
/* private: */
/*     bass_manager* bassm; */
/* }; */

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    void open_files();
    void add_to_playlist(QStringList songs_path);
    void play();
    void next_track();
    void prev_track();
    void volume_changed(int volume);
    void trackbar_worker();
    void trackbar_value_changed(int seconds);
    string format_seconds(int seconds);
    QTimer timer;
    Ui::MainWindow *ui;
    QStringListModel *model;
    bass_manager bassm;    
};
#endif // MAINWINDOW_H
